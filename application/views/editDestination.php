<?php
echo form_open_multipart('DestinationControl/editDestinationValidation')
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <ol class="breadcrumb breadcrumb-quirk">
      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewDestination'); ?>">Destination</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewDestination'); ?>">View Destination</a></li>
      <li class="active">Edit Destination</li>
    </ol>

    <div class="row">
      <div class=" col-md-12 col-lg-8">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Edit Tour Destination</h4>
            <p>You can edit destination for tour package here.</p>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="box">
                <img class="box1" src="<?php echo base_url('assets/DestinationImages/'.$destinationDetails['DestinationImage']);?>" alt="<?php $destinationDetails['DestinationImage'];?>">
              </div>    
            </div>          
            <div class="form-group">
              <span class="help-block">Tour Destination Name</span>
              <input type="text" name="DestinationName" placeholder="Tour Destination Name" class="form-control" value="<?php echo $destinationDetails['DestinationName'];?>" required />
            </div>
            <div class="help-block error"> 
              <?php echo form_error('DestinationName')?>
            </div>
            <div class="form-group">
                <span class="help-block">Edit Tour Destination Place Image.</span>
                <input class="custom-file-input" name="DestinationImage" type="file" accept="image/*"/>
            </div>
            <div class="help-block error">
              <?php echo form_error('DestinationImage')?>
              <!-- <?php //echo $ImageError; ?> -->
            </div>
            <input type="hidden" name="DestinationId" value="<?php echo $destinationDetails['DestinationId'];?>">
            <div class="">
              <button class="btn btn-success btn-quirk btn-wide">Save</button>
            </div>
          </div><!-- panel-body -->
        </div><!-- panel -->
      </div><!-- col-md-12 col-lg-8 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<style type="text/css">
  .error{
    color: red;
  }
   .custom-file-input::-webkit-file-upload-button 
  {
    background: #fff;
    border: 1px solid #bdc3d1;
    padding: 10px 12px;
  }
  .box{
    width: 250px;
    height: 170px;
  }
  .box1
  {
    width: 100%;
  }
</style>

<?php
echo form_close();
?>
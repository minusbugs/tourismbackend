<?php $this->load->view('layout/header.php');?>
  
  <div class="mainpanel">
  	<div class="contentpanel">
        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?php echo site_url('AdminLoginControl/viewHomeSlider'); ?>">Home Slider</a></li>
          <li class="active">View Home Slider</li>
        </ol>
        <div class="row">
        <?php if(isset($isDelete)){ 
        if($isDelete==1)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Home Slider Deleted</span>
                      <p>The Slider hasbeen deleted.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-20" class="gritter-item-wrapper with-icon times-circle danger" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Slider Cannot be Deleted</span>
                      <p>There is some server error. Try again later.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        
        } ?>
       
        <?php
        if($results!=NULL)
        {
        foreach($results  as $row)
          {?>
         <div class="col-md-3">
            <div class="card">
              <div class="panel">
              <div class="panel-body">
                <h4><b><?php echo $row->SliderTitle1?></b></h4>
                <h4><b><?php echo $row->SliderTitle2?></b></h4>  
                <h4><b><?php echo $row->SliderTitle3?></b></h4>               
                <hr/>
                <!-- <div class="panel-body"> -->
                  <p>
               <a href="<?php echo site_url('HomeSliderControl/editHomeSlider/'.$row->SliderId); ?>" class="btn-quirk"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn-quirk pull-right" data-toggle="modal" data-target="#myModal<?php echo $row->SliderId; ?>"><i class="fa fa-trash"></i> Delete</a>

                    <!-- Modal -->
                    <div class="modal bounceIn animated" id="myModal<?php echo $row->SliderId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Delete Hotel</h4>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo site_url('HomeSliderControl/deleteHomeSlider/'.$row->SliderId); ?>" type="button" class="btn btn-danger">Delete</a>
                          </div>
                        </div><!-- modal-content-->
                      </div><!-- modal-dialog -->
                     </div><!-- modal-->

                  </p>
                <!-- </div> -->
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>
          </div>
        <?php 
          }
        }
        else
        {
          echo "<h3>"."No Home Slider Added"."</h3>";
        }
        ?>
       </div><!-- row -->
      <ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php 
        } 
        ?>
      </ul>
     </div><!-- contentpanel -->
    </div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<style type="text/css">

</style>
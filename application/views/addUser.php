<?php
echo form_open_multipart('AccountControl/addUserValidation')
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
	<div class="contentpanel">
	    <ol class="breadcrumb breadcrumb-quirk">
	      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
	      <li class="active"><a href="<?php echo site_url('AdminLoginControl/addUser'); ?>">Add User</a></li>
	    </ol>
	    <div class="row">

	   <?php if(isset($isAddUser)){ 
	   	//echo "set";exit;
        if($isAddUser==1)
          { ?> 
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">User Added</span>
                      <p><?php echo $successMessage;?></p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-26" class="gritter-item-wrapper with-icon exclamation-circle warning" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">User Cannot Be Added.</span>      
                     <p><?php 
                          if($isAddUser==2) {echo $errorMessage2;}
                          else if($isAddUser==3){echo $errorMessage3;}
                          else if($isAddUser==4){echo $errorMessage4;}
                          else{echo $errorMessage5;};?></p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        } ?>

	    	<div class=" col-md-12 col-lg-8">
	        	<div class="panel">
	         		<div class="panel-heading">
	            		<h4 class="panel-title">Add New User</h4>
	            		<p>You can add new user and can share your privilage to them.</p>
	          		</div>
	          		<div class="panel-body">
	            		<div class="form-group">
	              			<input type="text" name="newUserName" placeholder="User Name" class="form-control" value='<?php echo set_value('newUserName')?>' required/>
	            		</div><!-- form-group -->
	            		<div class="form-group">
	              			<input type="password" name="newUserPassword" placeholder="Password" class="form-control" required/>
	            		</div><!-- form-group -->
	            		<div class="form-group">
	              			<input type="password" name="confirmNewUserPassword" placeholder="Confirm Password" class="form-control" required/>
	            		</div><!-- form-group -->
                  <div class="form-group">
                      <input type="email" name="newUserEmail" placeholder="Email" class="form-control" value='<?php echo set_value('newUserEmail')?>' required/>
                  </div><!-- form-group -->
                  <div class="form-group">
                      <input type="text" maxlength="10" name="newUserContactNumber" id="newUserContactNumber" onblur="return numbervalidate_addUser1(this,event);" onkeypress="return numbervalidate_addUser(this,event);" placeholder="Contact Number" class="form-control" value='<?php echo set_value('newUserContactNumber')?>' required/>
                      <span class="help-block" id="addUsernumber" style="color: red;"></span>
                  </div><!-- form-group -->
	            		<div class="form-group">
	            			 <button class="btn btn-success btn-quirk btn-wide">Save</button>
	            		</div>
	            	</div><!-- panel-body -->
	            </div><!-- panel -->
	        </div><!-- col-md-12 col-lg-8 -->
	    </div><!-- row -->
	</div><!-- contentpanel -->
</div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<?php
echo form_close();
?>
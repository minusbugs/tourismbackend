<?php
echo form_open_multipart('AccountControl/changePasswordValidation')
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
	<div class="contentpanel">
	    <ol class="breadcrumb breadcrumb-quirk">
	      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
	      <li class="active"><a href="<?php echo site_url('AdminLoginControl/changePassword'); ?>">Change Password</a></li>
	    </ol>
	    <div class="row">

	   <?php if(isset($isChange)){ 
	   	//echo "set";exit;
        if($isChange==1)
          { ?> 
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Password Changed</span>
                      <p>The password hasbeen changed.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-26" class="gritter-item-wrapper with-icon exclamation-circle warning" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Password Cannot be Changed.</span>
                      <p>There is mismatching in your inputs.Try again later.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        } ?>

	    	<div class=" col-md-12 col-lg-8">
	        	<div class="panel">
	         		<div class="panel-heading">
	            		<h4 class="panel-title">Change Account Password</h4>
	            		<p>You can change your account password here.</p>
	          		</div>
	          		<div class="panel-body">
	            		<div class="form-group">
	              			<input type="password" name="currentPassword" placeholder="Current Password" class="form-control" required/>
	            		</div><!-- form-group -->
	            		<div class="form-group">
	              			<input type="password" name="newPassword" placeholder="New Password" class="form-control" required/>
	            		</div><!-- form-group -->
	            		<div class="form-group">
	              			<input type="password" name="confirmNewPassword" placeholder="Confirm New Password" class="form-control" required/>
	            		</div><!-- form-group -->
	            		<div class="form-group">
	            			 <button class="btn btn-success btn-quirk btn-wide">Save</button>
	            		</div>
	            	</div><!-- panel-body -->
	            </div><!-- panel -->
	        </div><!-- col-md-12 col-lg-8 -->
	    </div><!-- row -->
	</div><!-- contentpanel -->
</div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<?php
echo form_close();
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <div class="row">
      <div class="col-md-12 col-lg-8 dash-left">
        <div class="row panel-statistics">

          <a href="<?php echo site_url('AdminLoginControl/viewDestination'); ?>">
            <div class="col-sm-6">
              <div class="panel panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-success">Destinations Added</h4>
                      <h3><?php echo $totalDestinations?></h3>
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->
          </a>

          <a href="<?php echo site_url('AdminLoginControl/viewPackage'); ?>">
            <div class="col-sm-6">
              <div class="panel panel-success-full panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-success">Packages Added</h4>
                      <h3><?php echo $totalPackages?></h3>
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->
          </a>

        

          <a href="<?php echo site_url('AdminLoginControl/viewHotel'); ?>">
            <div class="col-sm-6">
              <div class="panel panel-primary-full panel-updates">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-xs-7 col-lg-8">
                      <h4 class="panel-title text-success">Hotels Added</h4>
                      <h3><?php echo $totalHotel?></h3>
                    </div>
                  </div>
                </div>
              </div><!-- panel -->
            </div><!-- col-sm-6 -->
          </a>

        </div><!-- panel-statistics -->
      </div><!-- col-md-3 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

</section>

<?php $this->load->view('layout/footer.php');?>

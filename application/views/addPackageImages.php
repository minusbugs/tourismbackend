<?php
//echo form_open_multipart('PackageControl/addPackageImages')
?>  -->
<!-- <?php 
//echo form_open();
//echo form_hidden($PackageId); 
?> -->
<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('layout/header.php');?>
  <!-- <link rel="stylesheet" type="text/css" href="<?php //echo base_url('assets/css/dropzone.min.css') ?>"> -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/basic.min.css') ?>">
</head>
<body>
	<header>
	<div class="headerpanel">

    <div class="logopanel">
      <h2><a href="">Holiday Zone</a></h2>
    </div><!-- logopanel -->

    <div class="headerbar">

      <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>


      <div class="header-right">
        <ul class="headermenu">

        <li>
            <div class="btn-group">
              <button type="button" class="btn btn-logged" data-toggle="dropdown">
                <img src="<?php echo base_url();?>assets/images/photos/userLogo.PNG" alt="" />
                Admin
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu pull-right">
              <li><a href="<?php echo site_url('AdminLoginControl/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
              </ul>
            </div>
          </li>
           </ul>
      </div><!-- header-right -->
    </div><!-- headerbar -->
  </div><!-- header-->

	</header>


		<section>

  <div class="leftpanel">
    <div class="leftpanelinner">

      <!-- ################## LEFT PANEL PROFILE ################## -->

      <div class="media leftpanel-profile">
        <div class="media-left">
          <a href="#">
            <img src="<?php echo base_url();?>assets/images/photos/userlogo.PNG" alt="" class="media-object img-circle">
          </a>
        </div>
        <div class="media-body">
          <h4 class="media-heading">Admin <!--<a data-toggle="collapse" data-target="#loguserinfo" class="pull-right"><i class="fa fa-angle-down"></i></a>--></h4>
          <span>Tourism</span>
        </div>
      </div><!-- leftpanel-profile -->

      <div class="tab-content">

        <!-- ################# MAIN MENU ################### -->

        <div class="tab-pane active" id="mainmenu">
          <h5 class="sidebar-title">Main Menu</h5>
          <ul class="nav nav-pills nav-stacked nav-quirk">
            <li class=""><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?> "><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
           </ul>

           <ul class="nav nav-pills nav-stacked nav-quirk">
            <li class="nav-parent">
              <a href=""><i class="fa fa-road"></i> <span>Destination</span></a>
              <ul class="children">
                <li><a href="<?php echo site_url('AdminLoginControl/addDestination'); ?>">Add Destination</a></li>
                <!-- <li><a href="">View Destination</a></li> -->
                <!--<li><a href="">Delete Destination Package</a></li>-->
              </ul>
            </li>
            <li class="nav-parent active">
              <a href=""><i class="fa fa-suitcase"></i> <span>Package</span></a>
              <ul class="children">
                <li class="active"><a href="<?php echo site_url('AdminLoginControl/addPackage'); ?>">Add Package</a></li>
                <!-- <li><a href="">View Package</a></li> -->
                <!--<li><a href="">Delete Destination Package</a></li>-->
              </ul>
            </li>
            </ul>
        </div><!-- tab-pane -->
         </div><!-- tab-content -->

    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->

  <div class="mainpanel">

  		<div class="contentpanel">

      <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
        <li><a href="<?php echo site_url('AdminLoginControl/addPackage'); ?>">Packages</a></li>
        <li class="active">Add Package Images</li>
      </ol>

      <div class="row">
        <div class=" col-md-12 col-lg-8">

        	 <div class="panel">
            <div class="panel-heading">
              <h4 class="panel-title">Add More Images For Tour Package</h4>
              <p>You can add more images for tour package here.</p>
            </div>
            <input type = "hidden" id="PackageId" name = "PackageId" value = "<?php echo $PackageId; ?>" />
            <div class="panel-body">
              <div class="panel">
              <div class="panel-body">
              <span class="help-block">Add Package place image.</span>
              <div class="dropzone">
                <div class="dz-message">
                  <h3> Drag and Drop your images here Or Click here to upload</h3>
                </div>
              </div>
              </div><!-- panel-body-->
              </div><!-- panel -->

              <div class="col-sm-offset-5">
              	<a href="<?php echo site_url("PackageControl/index")?>"><button type="button" class="btn btn-default btn-quirk btn-wide">Finish</button></a>
                <button class="btn btn-success btn-quirk btn-wide status">Add Images</button>
              </div>

            </div><!-- panel-body-->
          </div><!-- panel -->

        </div><!-- col -->
       </div><!-- row-->
      </div><!--contentpanel -->
     </div><!-- mainpanel -->
     </section>
	<footer>
		<!-- <?php //$this->load->view('layout/footer.php');?> -->
	</footer>


<script src="<?php echo base_url();?>assets/lib/jquery/jquery.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo base_url();?>assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-autosize/autosize.js"></script>
<script src="<?php echo base_url();?>assets/lib/select2/select2.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-toggles/toggles.js"></script>
<script src="<?php echo base_url();?>assets/lib/jquery-maskedinput/jquery.maskedinput.js"></script>
<script src="<?php echo base_url();?>assets/lib/timepicker/jquery.timepicker.js"></script>
<script src="<?php echo base_url();?>assets/lib/dropzone/dropzone.js"></script>
<script src="<?php echo base_url();?>assets/lib/bootstrapcolorpicker/js/bootstrap-colorpicker.js"></script>
<script src="<?php echo base_url();?>assets/js/quirk.js"></script>
<script src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
<!-- <script src="<?php //echo base_url('assets/js/dropzone.min.js') ?>"></script> -->

<script type="text/javascript">
    var jPackageId=$('input:hidden[name=PackageId]').val();
    //var jPackageId=document.getElementById('PackageId').setAttribute('value', val);
    Dropzone.autoDiscover = false;
    var file= new Dropzone(".dropzone",{
      url: "<?php echo base_url('index.php/PackageControl/addPackageImages?id=') ?>"+jPackageId,
      // maxFilesize: 2,  // maximum size to uplaod 
      method:"post",
      acceptedFiles:"image/*", // allow only images
      paramName:"userfile",
      dictInvalidFileType:"Image files only allowed", // error message for other files on image only restriction 
      addRemoveLinks:true,
      autoProcessQueue: false
    });


    $('.status').click(function(){
      
        file.processQueue();
      
    });
//Upload file onchange 

file.on("sending",function(a,b,c){
  a.token=Math.random();
  c.append("token",a.token); //Random Token generated for every files 
});


// delete on upload 

file.on("removedfile",function(a){
  var token=a.token;
  $.ajax({
    type:"post",
    data:{token:token},
    url:"<?php echo base_url('upload/delete') ?>",
    cache:false,
    dataType: 'json',
    success: function(res){
      // alert('Selected file removed !');      

    }

  });
});


</script>


</body>
</html>

<!-- <?php 
//echo form_close();
?> -->
<!-- <?php// echo form_open();?>
  <?php //echo form_hidden('PackageId',$PackageId);?>
<?php //echo form_close();?>
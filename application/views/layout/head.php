<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <link rel="icon" href="<?php echo site_url();?>fav.ico">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/Hover/hover.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/fontawesome/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/weather-icons/css/weather-icons.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/jquery-ui/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/select2/select2.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/dropzone/dropzone.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/ionicons/css/ionicons.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/jquery-toggles/toggles-full.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/morrisjs/morris.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/jquery.gritter/jquery.gritter.css">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/lib/animate.css/animate.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/quirk.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/card.css">
	<title>Tourism Admin Backend</title>
</head>
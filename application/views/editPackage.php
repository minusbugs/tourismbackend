<?php
echo form_open_multipart('PackageControl/editPackageValidation');
foreach ($DestinationName as $key) 
{
  if($key->DestinationId == $packageDetails['DestinationId'])
  {
    $package=$key->DestinationName ;
  }
}
?>
	
<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <ol class="breadcrumb breadcrumb-quirk">
      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewPackage'); ?>">Package</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewPackage'); ?>">View Package</a></li>
      <li class="active">Edit Package</li>
    </ol>
    <div class="row">
      <div class=" col-md-12 col-lg-8">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Edit Tour Package</h4>
            <p>You can edit tour packages here.</p>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <div class="box">
                <img class="box1" src="<?php echo base_url('assets/PackageImages/'.$packageDetails['PackageImage']);?>" alt="<?php $packageDetails['PackageImage'];?>">
              </div>

            </div>
            <div class="form-group">
              <span class="help-block">Package Name</span>
              <input type="text" name="txtPackageName" placeholder="Tour Package Name" class="form-control" value="<?php echo $packageDetails['PackageTitle'];?>" required/>
            </div>

            <div class="form-group">
              <span class="help-block">Package Description</span>
              <!-- <input type="text" name="txtPackageDescription" placeholder="Tour Package Description" class="form-control" required /> -->
              <textarea id="autosize" name="txtPackageDescription" class="form-control" rows="5" placeholder="Tour Package Description" required><?php echo $packageDetails['PackageDescription'];?></textarea>
            </div>
            <div class="form-group">
      <select id="select2" name="selectPackageState" class="form-control" style="width: 100%" value="" required>
        <option value=""></option>
        <option value="Kerala" <?php if($packageDetails['PackageState']=="Kerala"){echo "selected";}?>>Kerala</option>
        <option value="Tamilnadu" <?php if($packageDetails['PackageState']=="Tamilnadu"){echo "selected";}?> >Tamilnadu</option>
        <option value="Karnataka" <?php if($packageDetails['PackageState']=="Karnataka"){echo "selected";}?>>Karnataka</option>
      </select>
            </div>
            <div class="form-group">
              <input type="text" name="txtPackageDays" value="<?=$packageDetails['Days']?>" class="form-control" required/>
            </div>
            <div class="form-group">
              <input type="text" name="txtPackageAmount" value="<?=$packageDetails['PackageAmount']?>/-" class="form-control" required/>
            </div>

            <div class="form-group">
              <span class="help-block">Tour Destination</span>
              <select id="select1" name="selectPackageDestination" class="form-control" style="width: 100%" data-placeholder="Tour Package Destination" required>
                <option value="<?php echo $packageDetails['DestinationId'];?>"><?php echo $package;?></option>
                   <?php  foreach($DestinationName as $key)
                  { ?>
                <option value="<?php echo  $key->DestinationId?> "> <?php echo $key->DestinationName ?> </option>;
                <?php  } ?>

              </select>
            </div>

           <!--  <div class="input-group">
              <span class="input-group-addon">₹</span>
              <input type="number" name="txtPackageAmount" class="form-control" placeholder="Tour Package Amount" value="<?php //echo $packageDetails['PackageAmount'];?>" required />
            </div> -->

            <!-- <div class="panel">
              <div class="panel-body"> -->
              <div class="form-group">
                <span class="help-block">Edit Package place image.</span>
                <form >
                    <input class="custom-file-input" name="filePackageImage" type="file"  accept="image/*" />
                </form>
              </div>
              <div class="panel-heading">
              <h4 class="panel-title">Add Package Destination Images</h4>
            </div>
            <div class="panel-body">
              <br /> 
              <form action="<?=base_url()?>PackageControl/upload" class="dropzone">
                <input type="hidden"  value="<?php echo $packageDetails['PackageId'];?>" name="txtHidden">
                <div class="fallback">
                  <input name="file" type="file" multiple />
                </div>
              </form>
            </div>
              <!-- </div>
            </div> -->
            <input type="hidden" name="PackageId" value="<?php echo $packageDetails['PackageId'];?>">
            <div class="">
              <button class="btn btn-success btn-quirk btn-wide">Save</button>
            </div>
          </div><!-- panel-heading -->
        </div><!-- panel -->

      </div><!-- col-md-12 col-lg-8 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

<style type="text/css">
  textarea
  {
    resize: none;
  }
  .extra-width
  {
    width: 500px
  }
  .custom-file-input::-webkit-file-upload-button 
  {
    background: #fff;
    border: 1px solid #bdc3d1;
    padding: 10px 12px;
  }
  .box
  {
    width: 250px;
    height: 170px;
    overflow: hidden;
  }
  .box1
  {
    width: 100%;
  }
</style>


<?php $this->load->view('layout/footer.php');?>

<?php
echo form_close();
?>
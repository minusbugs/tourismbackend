<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  		<div class="contentpanel">
	        <ol class="breadcrumb breadcrumb-quirk">
	          <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
	          <li><a href="<?php echo site_url('AdminLoginControl/viewPackage'); ?>">Package</a></li>
	          <li class="active">View Package</li>
	        </ol>
	   

		    <div class="panel">
		    	<div class="panel-heading">
		          <h4 class="panel-title">Basic Styling</h4>
		          <p>For basic styling add the base class <code>.table</code> to any table.</p>
		        </div>
		    </div>
		    <div class="panel-body">
			    <div class="row">
			    	<div class="col-md-3">
		            	<div class="card">
		              	 <img src="https://www.w3schools.com/howto/img_avatar.png" alt="Avatar" style="width:100%">
		              		<div class="panel-body">
				                <h4><b>John Doe</b></h4> 
				                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions </p> 
				            </div>
			            </div>
          			</div>
			    </div>
		    </div>
		</div>
</div>

<?php $this->load->view('layout/footer.php');?>
  
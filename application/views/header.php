<?php $this->load->view('layout/head.php');?>

<body>
	<header>
		<div class="headerpanel">
		    <div class="logopanel">
		      <h2><a href="">Holiday Zone</a></h2>
		    </div><!-- logopanel -->
		    <div class="headerbar">
		      <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
		      <div class="header-right">
		        <ul class="headermenu">
		          <li>
		            <div class="btn-group">
		              <button type="button" class="btn btn-logged" data-toggle="dropdown">
		                <img src="<?php echo base_url();?>assets/images/photos/userLogo.PNG" alt="" />
		                <?php echo $this->session->userdata('userType');?>
		                <span class="caret"></span>
		              </button>
		              <ul class="dropdown-menu pull-right">
		              <li><a href="<?php echo site_url('AdminLoginControl/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>		           
		              </ul>
		            </div>
		          </li>
		        </ul>
		      </div><!-- header-right -->
		    </div><!-- headerbar -->	  
	  	</div><!-- header-->
	</header>

	 <section>
	    <div class="leftpanel">
	    	<div class="leftpanelinner">
	        <!-- ################## LEFT PANEL PROFILE ################## -->
		    	<div class="media leftpanel-profile">
		          <div class="media-left">
		            <a href="#">
		              <img src="<?php echo base_url();?>assets/images/photos/userlogo.PNG" alt="" class="media-object img-circle">
		            </a>
		          </div>
		          <div class="media-body">
		            <h4 class="media-heading"><?php echo $this->session->userdata('userType');?> <!--<a data-toggle="collapse" data-target="#loguserinfo" class="pull-right"><i class="fa fa-angle-down"></i></a>--></h4>
		            <span>Tourism</span>
		          </div>
		        </div><!-- leftpanel-profile -->

		        <div class="tab-content">
		         <!-- ################# MAIN MENU ################### -->
		        	<div class="tab-pane active" id="mainmenu">
		            	<h5 class="sidebar-title">Main Menu</h5>
			            <ul class="nav nav-pills nav-stacked nav-quirk">
			              <li class="<?php if($navigationActive=="dashBoard"){ echo "active";} ?>"><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?> "><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
			            </ul>

			            <ul class="nav nav-pills nav-stacked nav-quirk">
				            <li class="nav-parent <?php if($navigationActive=="addDestination"||$navigationActive=="viewDestination"){ echo "active";} ?>">
				            	<a href=""><i class="fa fa-road"></i> <span>Destinations</span></a>
					            <ul class="children">
					            	<li class="<?php if($navigationActive=="addDestination"){ echo "active";} ?>"><a href="<?php echo site_url('AdminLoginControl/addDestination'); ?>">Add Destination</a></li>
					            	<li class="<?php if($navigationActive=="viewDestination"){ echo "active";} ?>"><a href="<?php echo site_url('AdminLoginControl/viewDestination'); ?>">View Destination</a></li>
					            </ul>
				             </li>
				             <li class="nav-parent <?php if($navigationActive=="viewPackage"||$navigationActive=="addPackage"){ echo "active";} ?> ">
				                <a href=""><i class="fa fa-suitcase"></i> <span>Packages</span></a>
				                <ul class="children">
				                  <li class="<?php if($navigationActive=="addPackage"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/addPackage'); ?>">Add Package</a></li>
				                  <li class="<?php if($navigationActive=="viewPackage"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/viewPackage'); ?>">View Package</a></li>				        
				                </ul>
				             </li>
				             <li class="nav-parent <?php if($navigationActive=="viewFlight"||$navigationActive=="addFlight"){ echo "active";} ?> ">
				                <a href=""><i class="fa fa-plane"></i> <span>Airport</span></a>
				                <ul class="children">
				                  <li class="<?php if($navigationActive=="addFlight"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/addFlight'); ?>">Add Flight</a></li>
				                  <li class="<?php if($navigationActive=="viewFlight"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/viewFlight'); ?>">View Flight</a></li>				        
				                </ul>
				             </li>
				             <li class="nav-parent <?php if($navigationActive=="viewHotel"||$navigationActive=="addHotel"){ echo "active";} ?> ">
				                <a href=""><i class="fa fa-hotel"></i> <span>Hotel</span></a>
				                <ul class="children">
				                  <li class="<?php if($navigationActive=="addHotel"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/addHotel'); ?>">Add Hotel</a></li>
				                  <li class="<?php if($navigationActive=="viewHotel"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/viewHotel'); ?>">View Hotel</a></li>				        
				                </ul>
				             </li>
				             <li class="nav-parent <?php if($navigationActive=="viewHomeSlider"||$navigationActive=="addHomeSlider"){ echo "active";} ?> ">
				                <a href=""><i class="fa fa-image"></i> <span>Home Slider</span></a>
				                <ul class="children">
				                  <li class="<?php if($navigationActive=="addHomeSlider"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/addHomeSlider'); ?>">Add Home Slider</a></li>
				                  <li class="<?php if($navigationActive=="viewHomeSlider"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/viewHomeSlider'); ?>">View Home Slider</a></li>				        
				                </ul>
				             </li>	
				             <?php 
				             if($this->session->userdata('userType')=="Admin")
				             {
				             	?>
				             <li class="<?php if($navigationActive=="addUser"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/addUser'); ?>"><i class="fa fa-users"></i><span>Users</span></a></li>					
				             <?php 
				             }
				             	?>
				             <li class="<?php if($navigationActive=="changePassword"){ echo "active";} ?> "><a href="<?php echo site_url('AdminLoginControl/changePassword'); ?>"><i class="fa fa-key"></i><span>Change Password</span></a></li>
				             <li><a href="<?php echo site_url('AdminLoginControl/logout'); ?>"><i class="glyphicon glyphicon-log-out"></i><span>Log out</span></a></li>
			              </ul>
		          	</div><!-- tab-pane -->
	           </div><!-- tab-content -->
	      	</div><!-- leftpanelinner -->
    	</div><!-- leftpanel -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class PackageControl extends CI_Controller 
	{

		function __construct()
	    {
	    	parent::__construct();
	    	$this->load->model('AdminLoginModel');
	    	$this->load->model('DestinationModel');
	    	$this->load->model('PackageModel');
	        $this->load->library('session');
	        $this->load->helper('form');
	    }

	    public function index()
	    {
	    	$data['DestinationName'] = $this->PackageModel->fetchDestinationSymbols();
			$data['navigationActive']="addPackage";
			$data['packid']=$this->PackageModel->getLastId();
			//print_r($this->PackageModel->getLastId());exit;	
	    	$this->load->view('addPackage',$data);

	    }

	    public function addPackage()
	    {
	    	$packid=$this->PackageModel->getLastId();
	    	if($packid==''){
	    		$packid=1;
	    	}else{
	    		$packid++;
	    	}
	    	$date = date("Y-m-d");
        	$time = Time();
        	$PackageName=$this->input->post('txtPackageName');
        	$PackageDescription=$this->input->post('txtPackageDescription');
        	$PackageDestination=$this->input->post('selectPackageDestination');
        	$PackageState=$this->input->post('selectPackageState');
        	$PackageDays=$this->input->post('txtPackageDays');
        	$PackageAmount=$this->input->post('txtPackageAmount');
        	//$PackageAmount=$this->input->post('txtPackageAmount');
        	$img= $_FILES['filePackageImage']['name'];
        	$images = explode('.',$img);
			$PackageImageName =$time.'.'.end($images);//.
	    	//$this->form_validation->set_rules('DestinationName','Destination Name','required');
	    	//$this->form_validation->set_rules('DestinationImage','Destination Image','required');
	  //   	if($this->form_validation->run()==false)
			// {
			// 	$this->load->view('addDestination');
			// }
			// else
			// {
			 	$config['upload_path']          = './assets/PackageImages/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		        //$config['max_size']             = 2000;
		        //$config['max_width']            = 1024;
		        //$config['max_height']           = 768;
		        $config['file_name'] = $PackageImageName;  
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('filePackageImage'))
		        {

					$ImageError = array('error' => $this->upload->display_errors(),'navigationActive'=>"addPackage",'DestinationName'=>$this->PackageModel->fetchDestinationSymbols(),);
					//print_r($ImageError);exit;
					$this->load->view('addPackage', $ImageError);
		        }
		        else
		        {
					$data=array('PackageId'=>$packid,
						'PackageTitle'=>$PackageName,'PackageDescription'=>$PackageDescription,'PackageImage'=>$PackageImageName,'DestinationId'=>$PackageDestination,'PackageState'=>$PackageState,
						'Days'=>$PackageDays,
						'PackageAmount'=>$PackageAmount);
					$returnId=$this->PackageModel->addPackage($data);
					$data=array('upload_data'=>$this->upload->data());
					// if($PackageId>0)
					// 	{
							//echo $PackageId;exit;
							//$this->morePackageImages($PackageId);
					$data["isAdd"]=$returnId;
					$data['DestinationName'] = $this->PackageModel->fetchDestinationSymbols();
					$data['navigationActive']="addPackage";
					$data['packid']=$this->PackageModel->getLastId();
	    			$this->load->view('addPackage',$data);
						// }
				}
	    }

	    public function editPackage($PackageId)
	    {
	    	//echo $PackageId;exit;
	    	$packageDetails=$this->PackageModel->fetchEditPackageDetails($PackageId);
	    	$data['packageDetails']=$packageDetails;
	    	$data['navigationActive']="addPackage";
	    	$data['DestinationName'] = $this->PackageModel->fetchDestinationSymbols();
	    	$this->load->view('editPackage',$data);
	    }
	    public function editPackageValidation()
	    {
	    	$PackageName=$this->input->post('txtPackageName');
        	$PackageDescription=$this->input->post('txtPackageDescription');
        	$PackageDestination=$this->input->post('selectPackageDestination');
        	$PackageAmount=$this->input->post('txtPackageAmount');
        	$PackageDays=$this->input->post('txtPackageDays');
        	$PackageState=$this->input->post('selectPackageState');
        	//$PackageAmount=$this->input->post('txtPackageAmount');
        	$PackageId=$this->input->post('PackageId');
        	$img= $_FILES['filePackageImage']['name'];
	    	//echo $DestinationId;exit;
	    	//echo $img;exit;
	    	if($img!="")
	    	{
	    		//echo "hii";exit;
	    		$date = date("Y-m-d");
	        	$time = Time();	
	        	$images = explode('.',$img);
	        	//print_r($images);exit;
				$PackageImageName =$time.'.'.end($images);
				$config['upload_path']          = './assets/PackageImages/';
			    $config['allowed_types']        = 'gif|jpg|png|jpeg';
			    $config['file_name'] = $PackageImageName;
			    $this->load->library('upload', $config);  
				$this->upload->initialize($config);
		    	if ($this->upload->do_upload('filePackageImage'))
		    	{
		    		//echo "hii";exit;
					$data=array('PackageTitle'=>$PackageName,'PackageDescription'=>$PackageDescription,'PackageImage'=>$PackageImageName,'DestinationId'=>$PackageDestination,'PackageAmount'=>$PackageAmount,'Days'=>$PackageDays,'PackageState'=>$PackageState);
					//print_r($data);exit;
					$result=$this->PackageModel->editPackage($data,$PackageId);
					$data=array('upload_data'=>$this->upload->data());
					$data['navigationActive']="viewPackage";
					$data['packageDetails']=$this->PackageModel->viewPackage();
			    	redirect('AdminLoginControl/viewPackage');
		    	}
		    }
	    	else
	    	{	
	    		//echo "hi";exit;
		    	$data=array('PackageTitle'=>$PackageName,'PackageDescription'=>$PackageDescription,'DestinationId'=>$PackageDestination,'PackageAmount'=>$PackageAmount,'Days'=>$PackageDays,'PackageState'=>$PackageState);
				$result=$this->PackageModel->editPackage($data,$PackageId);
				$data['navigationActive']="viewPackage";
				$data['packageDetails']=$this->PackageModel->viewPackage();

		    	redirect('AdminLoginControl/viewPackage');
		    }
	    }
	    public function deletePackage($PackageId)
	    {
	    	echo $PackageId;
	    	$PackageImage=$this->uri->segment(4);
	    	echo $PackageImage;
	    	$result=$this->PackageModel->deletePackage($PackageId);
	    	$data["isDelete"]=$result;
	    	if(file_exists("assets/PackageImages/".$PackageImage)){
	    		unlink("assets/PackageImages/".$PackageImage);
	    	}else{

	    	}
			$data['navigationActive']="viewPackage";
			$data['packageDetails']=$this->PackageModel->viewPackage();
			$this->session->set_flashdata('isDelete', $result);
			redirect('AdminLoginControl/viewPackage');
	    }
	    public function upload(){
	    	//$time=time();
	    	$packid=$this->input->post('txtHidden');
	    	$tempFile = $_FILES['file']['tmp_name'];
	        $fileName = $_FILES['file']['name'];
	        $targetPath = getcwd() . '/assets/PackageImages/';
	        $images = explode('.',$fileName);
			$imageName =rand(1000,100000).'.'.end($images);	        
	        //$targetFile = $targetPath . $packid."_".$fileName ;
	        move_uploaded_file($imageName, $targetFile);
	        $data = array('PackageId' =>$packid,
				       	  'ImageName'=>$imageName);
			$result=$this->PackageModel->uploadImages($data);
		}
		public function editpackageImages($packid){
			$data['PackageTitle']=$this->uri->segment(4);
			$data['navigationActive']="viewPackage";
			$data['results']=$this->PackageModel->getPackageImage($packid);
			$this->load->view('editPackageImages',$data);
		}
		public function deleteImage($imageid){
			$ImageName=$this->uri->segment(4);
			$PackageId=$this->PackageModel->getPackageIdImages($imageid);
			$result=$this->PackageModel->deleteImages($imageid);
			$data["isDelete"]=$result;
			if(file_exists("assets/PackageImages/".$ImageName)){
	    		unlink("assets/PackageImages/".$ImageName);
	    	}else{

	    	}
			$this->session->set_flashdata('isDelete', $result);
			if($PackageId==0){
				redirect('AdminLoginControl/viewPackage');
			}else{
				redirect('PackageControl/editpackageImages/'.$PackageId);
			}
		}
	}
?>
 function submitcontactmail()
        {
        var d = new FormData($("#contactform")[0]);
        var a='0';
        if($('#fullname').val() == "" || $('#email').val() == ""|| $('#message').val() == "")
        {
                        $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
        $.ajax({
                 type: "POST",
                data: d,
                url: "../Pages/send_mail",
                dataType: 'json',

                processData: false,
                contentType: false,
                success: function (result) {
                  var a = JSON.parse(result);

                  if (a == '1') 
                 {
                        $("#ajax-alert").addClass("alert  alert-success");                        
                        $("#ajax-alert").text("Mail Has been Send");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
                    $('#fullname').val('');
                    $("#email").val(' ');
                    $("#message").val(' ');
                  }
                  else
                   {
                   alert("Sending Failed");
                  }
                }
              })
        }
}
function submitPackageDetails()
{
   var d = new FormData($("#packagedetailsform")[0]);
        var a='0';
        if($('#txtFirtsName').val() == "" || $('#txtLastName').val() == ""|| $('#txtEmail').val() == "" || $('#txtMobileNo').val() == "")
        {
                        $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
  $.ajax({
        type: "POST",
        data: d,
        url: "../send_Package_Details",
        dataType: 'json',

        processData: false,
        contentType: false,
        success: function (result) {
          var a = JSON.parse(result);

          if (a == '1') 
         {

                $("#ajax-alert").addClass("alert  alert-success");
                $("#ajax-alert").text("Package Request Has been Send");
                $("#ajax-alert").alert();
                $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
              $('#txtFirtsName').val('');
              $("#txtLastName").val(' ');
              $("#txtEmail").val(' ');
              $("#txtMobileNo").val(' ');
              window.location.href = "../../Pages/packages";
          }
          else
           {
           alert("Sending Failed");
          }
        }
      })
  }
}

function submitflightmail()
{
     var formData = new FormData($("#flightform")[0]);
        var a='0';
        if($('#selectFlightFrom').val() == "" || $('#selectFlightTo').val() == ""|| $('#dateDeparting').val() == "" || $('#txtContactNumber').val() == "" || $('#selectAdult').val() == "" || $('#selectChild').val() == "")
        {
                        $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
     formData.append("FlightFrom", $("#selectFlightFrom").val());
     formData.append("FlightTo", $("#selectFlightTo").val());
     formData.append("Departing", $("#dateDeparting").val());
     formData.append("ContactNumber", $("#txtContactNumber").val());
     formData.append("Adult", $("#selectAdult").val());
     formData.append("Child", $("#selectChild").val());

  $.ajax({
        type: "POST",
        data: formData,
        url: "Pages/send_flight_mail",
        dataType: 'json',

        processData: false,
        contentType: false,
        success: function (result) {
          var a = JSON.parse(result);

          if (a == '1') 
         {
                $("#ajax-alert").addClass("alert  alert-success");
                $("#ajax-alert").text("Flight Request Has been Send");
                $("#ajax-alert").alert();
                $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
            $('#selectFlightFrom').val('');
            $("#selectFlightTo").val(' ');
            $("#dateDeparting").val(' ');
            $("#txtContactNumber").val(' ');
            $("#selectAdult").val(' ');
            $("#selectChild").val(' ');
          }
          else
           {
           alert("Sending Failed");
          }
        }
      })
   }
}
function submithotelmail()
{
     var formData = new FormData($("#hotelform")[0]);
        var a='0';
        if($('#selectHotelName').val() == "" || $('#dateCheckIn').val() == ""|| $('#dateChechOut').val() == "" || $('#txtContactNumber').val() == "")
        {
                        $("#ajax-alert").addClass("alert  alert-danger").text("Fill every field");
                        $("#ajax-alert").alert();
                        $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});               
        }
        else
        {
     formData.append("HotelName", $("#selectHotelName").val());
     formData.append("CheckIn", $("#dateCheckIn").val());
     formData.append("ChechOut", $("#dateChechOut").val());
     formData.append("ContactNumber", $("#txtContactNumber").val());

  $.ajax({
        type: "POST",
        data: formData,
        url: "Pages/send_hotel_mail",
        dataType: 'json',

        processData: false,
        contentType: false,
        success: function (result) {
          var a = JSON.parse(result);

          if (a == '1') 
         {
                $("#ajax-alert").addClass("alert  alert-success");
                $("#ajax-alert").text("Hotel Request Has been Send");
                $("#ajax-alert").alert();
                $("#ajax-alert").fadeTo(3000, 3000).slideUp(1000, function(){});
              $('#selectHotelName').val('');
              $("#dateCheckIn").val(' ');
              $("#dateChechOut").val(' ');
              $("#txtContactNumber").val(' ');
          }
          else
           {
           alert("Sending Failed");
          }
        }
      })
   }
}
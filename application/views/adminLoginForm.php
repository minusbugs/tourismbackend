<?php
echo form_open('AdminLoginControl/adminLoginValidation/');
?>

<?php $this->load->view('layout/head.php');?>

<?php if(isset($isLogin)){ 
        if($isLogin==0)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-20" class="gritter-item-wrapper with-icon times-circle danger" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Login Failed</span>
                      <p>Check your Username or Password</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
        } ?>
<body class="signwrapper">

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <div class="panel signin">
    <div class="panel-heading">
      <h1>Admin Login</h1>
      <h4 class="panel-title">Welcome! Please signin.</h4>
    </div>
    <div class="panel-body">
     <!-- <button class="btn btn-primary btn-quirk btn-fb btn-block">Connect with Facebook</button>
      <div class="or">or</div>-->
      <form>
        <div class="form-group mb10">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" name="UserName" class="form-control" placeholder="Enter Username" required="required" value="<?php echo set_value('UserName')?>">
          </div>
        </div>
        <div class="form-group mb10"><!-- class="nomargin" -->
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="Password" name="Password" class="form-control" placeholder="Enter Password" required="required">
          </div>
        </div>
        <!-- <div><a href="" class="forgot">Forgot password?</a></div> -->
        <div class="form-group">
          <button class="btn btn-success btn-quirk btn-block">Sign In</button>
        </div>
      </form>
      <hr class="invisible">
      <!--<div class="form-group">
        <a href="signup.html" class="btn btn-default btn-quirk btn-stroke btn-stroke-thin btn-block btn-sign">Not a member? Sign up now!</a>
      </div>
    </div>-->
    </div><!-- panel -->
  </div>
<?php $this->load->view('layout/footer.php');?>

<?php
echo form_close();
?>
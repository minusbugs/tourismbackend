<?php
echo form_open_multipart('DestinationControl/addDestinationValidation')
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <ol class="breadcrumb breadcrumb-quirk">
      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/addDestination'); ?>">Destination</a></li>
      <li class="active">Add Destination</li>
    </ol>

    <div class="row">

      <?php if(isset($isAdd))
      { 
        if($isAdd==1)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Destination Added</span>
                      <p>The Destination hasbeen added.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-26" class="gritter-item-wrapper with-icon exclamation-circle warning" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Destination Cannot be Added.</span>
                      <p>There is some server issue.Try again later.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        } ?>

      <div class=" col-md-12 col-lg-8">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Add Tour Destination</h4>
            <p>You can add new destination for tour package here.</p>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <input type="text" name="DestinationName" placeholder="Tour Destination Name" class="form-control" required />
            </div>
            <div class="help-block error">
              <?php echo form_error('DestinationName')?>
            </div>
             <div class="form-group">
                <span class="help-block">Add Destination place image.</span>
                <form >
                    <input class="custom-file-input" name=" DestinationImage" type="file"  accept="image/*" required />
                </form>
              </div>
            <div class="help-block error">
              <?php echo form_error('DestinationImage')?>
              <!-- <?php //echo $ImageError; ?> -->
            </div>
            <div class="">
              <button class="btn btn-success btn-quirk btn-wide">Save</button>
            </div>
          </div><!-- panel-body -->
        </div><!-- panel -->
      </div><!-- col-md-12 col-lg-8 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<style type="text/css">
  .error{
    color: red;
  }

 .custom-file-input::-webkit-file-upload-button 
  {
    background: #fff;
    border: 1px solid #bdc3d1;
    padding: 10px 12px;
  }
</style>

<?php
echo form_close();
?>
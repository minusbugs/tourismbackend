<?php $this->load->view('layout/header.php');?>
  
  <div class="mainpanel">
  	<div class="contentpanel">
        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?php echo site_url('AdminLoginControl/viewUser'); ?>">Users</a></li>
          <li class="active">View User</li>
        </ol>
        <div class="row">
        <?php if(isset($isDelete)){ 
        if($isDelete==1)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">User Deleted</span>
                      <p>The User hasbeen deleted.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-20" class="gritter-item-wrapper with-icon times-circle danger" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">User Cannot be Deleted</span>
                      <p>There is some server error. Try again later.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        
        } ?>
       
        <?php
        if($results!=NULL)
        {
        foreach($results as $row)
          {?>
              <div class="col-md-3 col-lg-3">
                <div class="panel panel-profile grid-view">
                  <div class="panel-heading">
                    <div class="text-center">
                      <h4 class="panel-profile-name"><?php echo $row->AdminName?></h4>
                      <p class="media-usermeta"><i class="glyphicon glyphicon-briefcase"></i> Guest User</p>
                    </div>
                   <!--  <ul class="panel-options">
                      <li><a class="tooltips" href="" data-toggle="tooltip" title="View Options"><i class="glyphicon glyphicon-option-vertical"></i></a></li>
                    </ul> -->
                  </div><!-- panel-heading -->
                  <div class="panel-body people-info">
                    <div class="info-group">
                      <label>Email</label>
                      <?php echo $row->AdminEmail?>
                    </div>
                    <div class="info-group">
                      <label>Phone</label>
                      <?php echo $row->AdminContactNumber?>
                      <hr/>
                    </div>
                    
                <!-- <div class="panel-body"> -->
                  <p>
                    <!-- <a href="<?php //echo site_url('FlightControl/editFlight/'.$row->AirportId); ?>" class="btn-quirk"><i class="fa fa-edit"></i> Edit</a> -->
                    <a href="" class="btn-quirk pull-right" data-toggle="modal" data-target="#myModal<?php echo $row->AdminId; ?>"><i class="fa fa-trash"></i> Delete</a>

                    <!-- Modal -->
                    <div class="modal bounceIn animated" id="myModal<?php echo $row->AdminId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo site_url('AccountControl/deleteUser/'.$row->AdminId); ?>" type="button" class="btn btn-danger">Delete</a>
                          </div>
                        </div><!-- modal-content-->
                      </div><!-- modal-dialog -->
                     </div><!-- modal-->

                  </p>
                  </div><!-- panel-body -->
                </div><!-- panel -->
              </div><!-- col-md-6 -->
        <?php 
          }
        }
        else
        {
          echo "<h3>"."No other Users Added"."</h3>";
        }
        ?>
       </div><!-- row -->
      <ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php 
        } 
        ?>
      </ul>
     </div><!-- contentpanel -->
    </div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<style type="text/css">

.img-fix
{
  height:150px;
  background: rgb(255,255,255,0.5);
  overflow: hidden;
}
img
{
  width: 100%;  
}
</style>
<?php
echo form_open_multipart('PackageControl/addPackage')
?>
	
<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <ol class="breadcrumb breadcrumb-quirk">
      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/addPackage'); ?>">Package</a></li>
      <li class="active">Add Package</li>
    </ol>
    <div class="row">

      <?php if(isset($isAdd)){ 
        if($isAdd==1)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Package Added</span>
                      <p>The package hasbeen added.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-26" class="gritter-item-wrapper with-icon exclamation-circle warning" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Package Cannot be Added.</span>
                      <p>There is some server issue.Try again later.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        } ?>

      <div class=" col-md-12 col-lg-8">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Add Tour Package</h4>
            <p>You can add new tour packages here.</p>
          </div>
          <div class="panel-body">
              <div class="form-group">
                <input type="text" name="txtPackageName" placeholder="Tour Package Name" class="form-control" required/>
              </div>

            <div class="form-group">
              <!-- <input type="text" name="txtPackageDescription" placeholder="Tour Package Description" class="form-control" required /> -->
              <textarea id="autosize" name="txtPackageDescription" class="form-control" rows="5" placeholder="Tour Package Description" required></textarea>
            </div>
            <div class="form-group">
              <select id="select2" name="selectPackageState" class="form-control" style="width: 100%" data-placeholder="Tour Package State" required>
                <option value="">&nbsp;</option>
                <option value="Kerala">Kerala</option>
                <option value="Tamilnadu">Tamilnadu</option>
                <option value="Karnataka">Karnataka</option>
              </select>
            </div>
            <div class="form-group">
              <input type="text" name="txtPackageDays" placeholder="Tour Package Days" class="form-control" required/>
            </div>
            <div class="form-group">
              <input type="text" name="txtPackageAmount" placeholder="Tour Package Amount" class="form-control" required/>
            </div>

            <div class="form-group">
              <select id="select1" name="selectPackageDestination" class="form-control" style="width: 100%" data-placeholder="Tour Package Destination" required>
                <option value="">&nbsp;</option>
                   <?php  foreach($DestinationName as $key)
                  { ?>
                <option value="<?php echo  $key->DestinationId?> "> <?php echo $key->DestinationName ?> </option>;
                <?php  } ?>

              </select>
            </div>
          
           <!--  <div class="input-group">
              <span class="input-group-addon">₹</span>
              <input type="number" name="txtPackageAmount" class="form-control" placeholder="Tour Package Amount" required />
            </div> -->

            <!-- <div class="panel">
              <div class="panel-body"> -->
              <div class="form-group">
                <span class="help-block">Add Package place image.</span>
                <form >
                    <input class="custom-file-input" name="filePackageImage" type="file"  accept="image/*" required />
                </form>
              </div>
              <div class="panel">
            <div class="panel-heading">
              <h4 class="panel-title">Add Package Destination Images</h4>
            </div>
            <div class="panel-body">
              <br /> 
              <form action="<?=base_url()?>PackageControl/upload" class="dropzone">
                <input type="hidden"  value="<?php $packid++; echo $packid ; ?>" name="txtHidden">
                <div class="fallback">
                  <input name="file" type="file" multiple />
                </div>
              </form>
            </div>
          </div>
              <!-- </div>
            </div> -->
            
            <div class="">
              <button class="btn btn-success btn-quirk btn-wide">Save</button>
            </div>
          </div><!-- panel-heading -->
        </div><!-- panel -->

      </div><!-- col-md-12 col-lg-8 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

<style type="text/css">
  textarea
  {
    resize: none;
  }
  .extra-width
  {
    width: 500px
  }
  .custom-file-input::-webkit-file-upload-button 
  {
    background: #fff;
    border: 1px solid #bdc3d1;
    padding: 10px 12px;
  }
</style>


<?php $this->load->view('layout/footer.php');?>

<?php
echo form_close();
?>
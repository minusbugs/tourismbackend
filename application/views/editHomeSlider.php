<?php
echo form_open_multipart('HomeSliderControl/editHomeSliderValidation')
?>

<?php $this->load->view('layout/header.php');?>

<div class="mainpanel">
  <div class="contentpanel">
    <ol class="breadcrumb breadcrumb-quirk">
      <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewHomeSlider'); ?>"></a>Home Slider</li>
      <li><a href="<?php echo site_url('AdminLoginControl/viewHomeSlider'); ?>">View Home Slider</a></li>
      <li class="active">Edit Home Slider</li>
    </ol>

    <div class="row">
      <div class=" col-md-12 col-lg-8">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Edit Home Slider</h4>
            <p>You can edit home slider details here.</p>
          </div>
          <div class="panel-body">         
            <div class="form-group">
              <span class="help-block">Home Slider Title1</span>
              <input type="text" name="txtHomeSlider1" placeholder="Home Slider Title1" class="form-control" value="<?php echo $homesliderDetails['SliderTitle1'];?>" required />
            </div>
            <div class="form-group">
              <span class="help-block">Home Slider Title2</span>
              <input type="text" name="txtHomeSlider2" placeholder="Home Slider Title2" class="form-control" value="<?php echo $homesliderDetails['SliderTitle2'];?>" required />
            </div>
            <div class="form-group">
              <span class="help-block">Home Slider Title3</span>
              <input type="text" name="txtHomeSlider3" placeholder="Home Slider Title3" class="form-control" value="<?php echo $homesliderDetails['SliderTitle3'];?>" required />
            </div>                        
            <input type="hidden" name="SliderId" value="<?php echo $homesliderDetails['SliderId'];?>">
            <div class="">
              <button class="btn btn-success btn-quirk btn-wide">Save</button>
            </div>
          </div><!-- panel-body -->
        </div><!-- panel -->
      </div><!-- col-md-12 col-lg-8 -->
    </div><!-- row -->
  </div><!-- contentpanel -->
</div><!-- mainpanel -->

<?php $this->load->view('layout/footer.php');?>

<style type="text/css">
  .error{
    color: red;
  }
</style>

<?php
echo form_close();
?>
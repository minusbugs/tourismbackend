<?php $this->load->view('layout/header.php');?>
  
  <div class="mainpanel">
  	<div class="contentpanel">
        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="<?php echo site_url('AdminLoginControl/dashBoard'); ?>"><i class="fa fa-home mr5"></i> Home</a></li>
          <li><a href="<?php echo site_url('AdminLoginControl/viewDestination'); ?>">Destination</a></li>
          <li class="active">View Destination</li>
        </ol>
        <div class="row">
        <?php if(isset($isDelete)){ 
        if($isDelete==1)
          { ?>
            <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-24" class="gritter-item-wrapper with-icon check-circle success" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Destination Deleted</span>
                      <p>The destination hasbeen deleted.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>
          <?php   
          }
          else
            { ?>
              <div id="gritter-notice-wrapper" class="alert">
                <div id="gritter-item-20" class="gritter-item-wrapper with-icon times-circle danger" style="" role="alert">
                  <div class="gritter-top"></div>
                  <div class="gritter-item">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >x</button>
                    <div class="gritter-without-image">
                      <span class="gritter-title">Destination Cannot be Deleted</span>
                      <p>There is some package goes under this destination.</p>
                    </div>
                    <div style="clear:both"></div>
                  </div>
                  <div class="gritter-bottom"></div>
                </div>
              </div>

          <?php
            }
        
        } ?>
       
        <?php
        if($results!=NULL)
        {
        foreach($results  as $row)
          {?>
         <div class="col-md-3">
            <div class="card">
            <div class="img-fix" onclick="popup_destination(<?php echo $row->DestinationId; ?>);">
              <img id="destinationimage_<?php echo $row->DestinationId; ?>" class="img-fix1" src="<?php echo base_url('assets/DestinationImages/'.$row->DestinationImage);?>" alt="<?php $row->DestinationImage?>">
            </div>
              
              <div class="panel">
              <div class="panel-body">
                <h4 id="destinationtitle_<?php echo $row->DestinationId; ?>"><b><?php echo $row->DestinationName?></b></h4> 
                <!-- <div class="panel-body">
                  <p><?php //echo $row->PackageDescription?></p>
                </div> -->
                <hr/>
                <!-- <div class="panel-body"> -->
                  <p>
                    <a href="<?php echo site_url('DestinationControl/editDestination/'.$row->DestinationId); ?>" class="btn-quirk"><i class="fa fa-edit"></i> Edit</a>
                    <a href="" class="btn-quirk pull-right" data-toggle="modal" data-target="#myModal<?php echo $row->DestinationId; ?>"><i class="fa fa-trash"></i> Delete</a>

                    <!-- Modal -->
                    <div class="modal bounceIn animated" id="myModal<?php echo $row->DestinationId; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Delete Destination</h4>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo site_url('DestinationControl/deleteDestination/'.$row->DestinationId); ?>" type="button" class="btn btn-danger">Delete</a>
                          </div>
                        </div><!-- modal-content-->
                      </div><!-- modal-dialog -->
                     </div><!-- modal-->

                  </p>
                <!-- </div> -->
                </div><!-- panel-body -->
              </div><!-- panel -->
            </div>
          </div>
        <?php 
          }
        }
        else
        {
          echo "<h3>"."No Destination Added"."</h3>";
        }
        ?>
       </div><!-- row -->
      <!-- <div id="pagination">
        <ul class="tsc_pagination"> -->
        <!-- Show pagination links -->
       <!--  <?php //foreach ($links as $link) 
        {
         // echo "<li>". $link."</li>";
        } ?>
        </ul>
      </div>
 -->

      <ul class="pagination pull-right">
        <?php foreach ($links as $link) 
        {
          ?><li><?php echo $link?></li>
          <?php 
        } 
        ?>
      </ul>
     </div><!-- contentpanel -->
    </div><!-- mainpanel -->
  <!-- Modal View-->
  <div class="row">
  <div class="modal fadeIn animated" id="myModalView" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
       <!--  <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         
        </div> -->
        <div class="modal-body">
          <img class="img-fix3" id="myModalLabel_image">  
                      
        <div class="modal-footer">  
          <h3 class="center" id="myModalLabel_head"></h3>                  
        </div>
        </div> 
      </div><!-- modal-content-->
    </div><!-- modal-dialog-->
  </div><!-- modal-->
  </div>
<?php $this->load->view('layout/footer.php');?>

<style type="text/css">

.img-fix
{
  height:150px;
  background: rgb(255,255,255,0.5);
  overflow: hidden;
  cursor: pointer;
}
.img-fix:hover
{
  opacity: .5;
  transition: 2ms;
}
.img-fix1
{
  width: 100%; 
  height: 100%; 
}
.img-fix3
{
  width: 100%; 
}
.center
{
  text-align: justify; 
}
</style>
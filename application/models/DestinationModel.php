<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DestinationModel extends CI_Model {
	// public function adminLoginValidation($AdminName,$AdminPassword)
	// {
	// 	$this->load->library('session');
	// 	$this->db->where(array('AdminName'=>$AdminName,'AdminPassword'=>$AdminPassword));
	// 		$count=$this->db->count_all_results('admin_db');
	// 		return $count;
	// }
	public function addDesination($Desination)
	{
		$returnId=0;
		$returnId=$this->db->insert('destinations',$Desination);
		return $returnId;
	}
	public function destinationCount() 
	{
		return $this->db->count_all("destinations");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("destinations");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function viewDestination()
	{
		$this->db->select('*');
	    $this->db->from('destinations');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deleteDestination($DestinationId)
	{
		//echo $DestinationId;exit;
		$this->db->where(array('DestinationId'=>$DestinationId,));
		$count=$this->db->count_all_results('packages');
		$returnId=0;
		if($count<=0)
		{
			$this->db->where('DestinationId',$DestinationId);
			$returnId= $this->db->delete('destinations');
		}

		return $returnId;
		
	}
	public function fetchEditDestinationDetails($DestinationId)
	{
		$query=$this->db->get_where('destinations',array('DestinationId'=>$DestinationId));
		return $query->row_array();
	}
	public function editDestination($data,$DestinationId)
	{
		$this->db->where('DestinationId',$DestinationId);
		$res=$this->db->update('destinations',$data);
		return $res;
	}
}
?>
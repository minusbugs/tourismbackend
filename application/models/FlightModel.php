<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FlightModel extends CI_Model 
{
	public function countFlight()
	{
		$count=$this->db->count_all_results('airport');
		return $count;
	}
	public function addFlight($Flight)
	{
		$returnId=$this->db->insert('airport',$Flight);
		return $returnId;
	}
	public function flightCount() 
	{
		return $this->db->count_all("airport");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("airport");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	public function viewFlight()
	{
		$this->db->select('*');
	    $this->db->from('airport');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deleteFlight($FlightId)
	{
		//echo $DestinationId;exit;
		$returnId=0;
		$this->db->where('AirportId',$FlightId);
		$returnId=$this->db->delete('airport');
		return $returnId;
	}
	public function fetchEditFlightDetails($FlightId)
	{
		$query=$this->db->get_where('airport',array('AirportId'=>$FlightId));
		return $query->row_array();
	}
	public function editFlight($data,$FlightId)
	{
		$this->db->where('AirportId',$FlightId);
		$res=$this->db->update('airport',$data);
		return $res;
	}
}
?>
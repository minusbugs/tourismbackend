<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeSliderModel extends CI_Model 
{
	public function countHomeSlider()
	{
		$count=$this->db->count_all_results('homeslider');
		return $count;
	}
	public function addHomeSlider($HomeSlider)
	{
		$returnId=$this->db->insert('homeslider',$HomeSlider);
		return $returnId;
	}
	public function homesliderCount() 
	{
		return $this->db->count_all("homeslider");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("homeslider");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	public function viewHomeSlider()
	{
		$this->db->select('*');
	    $this->db->from('homeslider');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deleteHomeSlider($HomeSliderId)
	{
		//echo $DestinationId;exit;
		$returnId=0;
		$this->db->where('SliderId',$HomeSliderId);
		$returnId=$this->db->delete('homeslider');
		return $returnId;
	}
	public function fetchEditHomeSliderDetails($HomeSliderId)
	{
		$query=$this->db->get_where('homeslider',array('SliderId'=>$HomeSliderId));
		return $query->row_array();
	}
	public function editHomeSlider($data,$HomeSliderId)
	{
		$this->db->where('SliderId',$HomeSliderId);
		$res=$this->db->update('homeslider',$data);
		return $res;
	}
}
?>
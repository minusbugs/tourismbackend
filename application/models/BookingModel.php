<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BookingModel extends CI_Model{
	
	public function bookingCount(){
		return $this->db->count_all('package_booking');
	}
	public function getBookingData($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("package_booking");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
}

?>
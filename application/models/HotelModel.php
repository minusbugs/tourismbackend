<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HotelModel extends CI_Model 
{
	public function countHotel()
	{
		$count=$this->db->count_all_results('hotel');
		return $count;
	}
	public function addHotel($Hotel)
	{
		$returnId=$this->db->insert('hotel',$Hotel);
		return $returnId;
	}
	public function hotelCount() 
	{
		return $this->db->count_all("hotel");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("hotel");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	public function viewHotel()
	{
		$this->db->select('*');
	    $this->db->from('hotel');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deleteHotel($HotelId)
	{
		//echo $DestinationId;exit;
		$returnId=0;
		$this->db->where('HotelId',$HotelId);
		$returnId=$this->db->delete('hotel');
		return $returnId;
	}
}
?>
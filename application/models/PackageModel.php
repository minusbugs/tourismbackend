<?php
Class PackageModel extends CI_Model
{
	function fetchDestinationSymbols()
	{
	        $this->db->select('*');
	        $this->db->from('destinations');
	        $query = $this->db->get();
	        return $query->result();
	}

	public function addPackage($Package)
	{
		$returnId=0;
		$returnId=$this->db->insert('packages',$Package);
		return $returnId;
	//$PackageId=$this->db->insert_id();//getting package id to add more package images
	//echo $PackageId;exit;
	//return $PackageId;
	}
	public function packageCount() 
	{
		return $this->db->count_all("packages");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		//$this->db->where('DestinationId', $id);
		$query = $this->db->get("packages");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function viewPackage()
	{
		$this->db->select('*');
	    $this->db->from('packages');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deletePackage($PackageId)
	{
		$returnId=0;
		$this->db->where('PackageId',$PackageId);
		$returnId=$this->db->delete('packages');
		return $returnId;
	}
	public function fetchEditPackageDetails($PackageId)
	{
		$query=$this->db->get_where('packages',array('PackageId'=>$PackageId));
		return $query->row_array();
	}
	public function editPackage($data,$PackageId)
	{
		$this->db->where('PackageId',$PackageId);
		$res=$this->db->update('packages',$data);
		return $res;
	}
	public function getLastId(){
		$this->db->select('PackageId');
		$this->db->from('packages');
		$this->db->order_by('PackageId','desc');
		$query=$this->db->get();
		return $query->row()->PackageId;
	}
	public function uploadImages($Package){
		if($this->db->insert('package_images',$Package)){
			return "true";
		}else{
			return "false";
		}
	}
	public function getPackageImage($packid){
		$this->db->select('*');
		$this->db->where('PackageId',$packid);
		$this->db->from('package_images');
		$query=$this->db->get();
		return $query->result();
	}
	public function deleteImages($imageid){
		$this->db->where('ImageId',$imageid);
		return $this->db->delete('package_images');
	}
	public function getPackageIdImages($imageid){
		$this->db->select('PackageId');
		$this->db->where('ImageId',$imageid);
		$this->db->from('package_images');
		$query=$this->db->get();
		return $query->row()->PackageId;
	}
}
?>
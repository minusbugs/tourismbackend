<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountControlModel extends CI_Model 
{
	public function fetchOldPassword($currentPassword)
	{
		$this->db->where('AdminName',$this->session->userdata('AdminName'));
    	$query=$this->db->get('admin_db');
    	if($query->num_rows()>0)
    	{
        	$row=$query->row();
        	if($currentPassword == $row->AdminPassword)
        	{
            	return 1;
        	}
        	else
        	{
            	return 0;
        	}
		}
		else
		{
			return 0;
		}
	}
	public function changePassword($newPassword)
	{
	    $data = array(
	           'AdminPassword' => $newPassword
	        );
	    $this->db->where('AdminName', $this->session->userdata('AdminName'));
	    $this->db->update('admin_db', $data);
	    return 1;
	}
	public function addUser($data)
	{
		$returnId=0;
		$returnId=$this->db->insert('admin_db',$data);
		return $returnId;
	}
	public function userCount() 
	{
		return $this->db->count_all("admin_db");
	}
	public function fetch_data($limit, $id) 
	{
		if($id>1)
		{
			$offset = ($id-1)*$limit;
			$this->db->limit($limit,$offset);
		}
		else
		{
			$this->db->limit($limit,$id);
		}
		$this->db->where('UserType',"Guest");
		$query = $this->db->get("admin_db");
		if ($query->num_rows() > 0) 
		{
			
			foreach ($query->result() as $row) 
			{

				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	public function viewUser()
	{
		$this->db->select('*');
	    $this->db->from('admin_db');
	    $query = $this->db->get();
	    return $query->result();
	}
	public function deleteUser($AdminId)
	{
		$returnId=0;
		$this->db->where('AdminId',$AdminId);
		$returnId=$this->db->delete('admin_db');
		return $returnId;
	}
}
?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminLoginModel extends CI_Model {
	public function adminLoginValidation($AdminName,$AdminPassword)
	{
		$this->load->library('session');
		$this->db->where(array('AdminName'=>$AdminName,'AdminPassword'=>$AdminPassword));
		$count=$this->db->count_all_results('admin_db');
		return $count;
	}
	public function userType($AdminName)
	{
		$query=$this->db->get_where('admin_db',array('AdminName'=>$AdminName));
		$row=$query->row();
		return $row->UserType;
	}
	public function countDestination()
	{
		$count=$this->db->count_all_results('destinations');
		return $count;
	}
	public function countPackages()
	{
		$count=$this->db->count_all_results('packages');
		return $count;
	}
}
?>
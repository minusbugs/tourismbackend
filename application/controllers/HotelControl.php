<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class HotelControl extends CI_Controller 
	{

		function __construct()
	    {
	    	parent::__construct();
	    	$this->load->model('AdminLoginModel');
	    	$this->load->model('DestinationModel');
	    	$this->load->model('PackageModel');
	    	$this->load->model('FlightModel');
	    	$this->load->model('HotelModel');
	        $this->load->library('session');
	        $this->load->library('upload');
	        $this->load->library('pagination');
	    }
	   	public function index()
	    {
	    	$data['navigationActive']="addHotel";
	    	$this->load->view('addHotel',$data);
	    } 
	    public function addHotel()
	    {
	    	$HotelName=$this->input->post('txtHotelName');
	    	$data=array('HotelName'=>$HotelName);
	    	$returnId=$this->HotelModel->addHotel($data);
	    	$data["isAdd"]=$returnId;
			$data['navigationActive']="addHotel";
	    	$this->load->view('addHotel',$data);
	    }
	   	public function deleteHotel($HotelId)
	    {
	    	//echo $PackageId;exit;
	    	//$vaildDeleteDestination=$this->vaildDeleteDestination($DestinationId);
	    	$result=$this->HotelModel->deleteHotel($HotelId);
			//$data["isDelete"]=$result;		
			$data['navigationActive']="viewHotel";
			$data['hotelDetails']=$this->HotelModel->viewHotel();
			$this->session->set_flashdata('isDelete', $result);
			redirect('AdminLoginControl/viewHotel');
			//$this->load->view('viewDestination',$data);			
	    }
	}
?>
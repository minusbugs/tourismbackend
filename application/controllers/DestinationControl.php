<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class DestinationControl extends CI_Controller 
	{

		function __construct()
	    {
	    	parent::__construct();
	    	$this->load->model('AdminLoginModel');
	    	$this->load->model('DestinationModel');
	    	$this->load->model('PackageModel');
	        $this->load->library('session');
	        $this->load->library('upload');
	        $this->load->library('pagination');
	    }
	    public function index()
	    {
	    	$data['navigationActive']="addDestination";
	    	$this->load->view('addDestination',$data);
	    }
	    public function addDestinationValidation()
	    {
	    	$date = date("Y-m-d");
        	$time = Time();
        	$DestinationName=$this->input->post('DestinationName');
			$img= $_FILES['DestinationImage']['name'];
        	$images = explode('.',$img);
			$DestinationImageName =$time.'.'.end($images);
	    	//$this->form_validation->set_rules('DestinationName','Destination Name','required');
	    	//$this->form_validation->set_rules('DestinationImage','Destination Image','required');
	  //   	if($this->form_validation->run()==false)
			// {
			// 	$this->load->view('addDestination');
			// }
			// else
			// {
			 	$config['upload_path']          = './assets/DestinationImages/';
		        $config['allowed_types']        = 'gif|jpg|png|jpeg';
		        //$config['max_size']             = 2000;
		        //$config['max_width']            = 1024;
		        //$config['max_height']           = 768;
		        $config['file_name'] = $DestinationImageName;  
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if ( ! $this->upload->do_upload('DestinationImage'))
		        {
					$ImageError = array('error' => $this->upload->display_errors(),'navigationActive'=>"addDestination",);
					$this->load->view('addDestination', $ImageError);
		        }
		        else
		        {
					$data=array('DestinationName'=>$DestinationName,'DestinationImage'=>$DestinationImageName);
					$returnId=$this->DestinationModel->addDesination($data);
					$data=array('upload_data'=>$this->upload->data());
					$data["isAdd"]=$returnId;
					$data['navigationActive']="addDestination";
	    			$this->load->view('addDestination',$data);	
				}
	    }
	   	public function editDestination($DestinationId)
	    {
	    	//echo $DestinationId;exit;
	    	$destinationDetails=$this->DestinationModel->fetchEditDestinationDetails($DestinationId);
	    	$data['destinationDetails']=$destinationDetails;
	    	$data['navigationActive']="addDestination";
	    	$this->load->view('editDestination',$data);
	    }
	    public function editDestinationValidation()
	    {
	    	$DestinationName=$this->input->post('DestinationName');	
	    	$DestinationId=$this->input->post('DestinationId');
	    	//echo $DestinationId;exit;
	    	$img= $_FILES['DestinationImage']['name'];
	    	//echo $img;exit;
	    	if($img!="")
	    	{
	    		//echo "hii";exit;
	    		$date = date("Y-m-d");
	        	$time = Time();	
	        	$images = explode('.',$img);
	        	//print_r($images);exit;
				$DestinationImageName =$time.'.'.end($images);
				//echo $DestinationImageName;exit;
				$config['upload_path']          = './assets/DestinationImages/';
			    $config['allowed_types']        = 'gif|jpg|png|jpeg';
			    $config['file_name'] = $DestinationImageName;
			    $this->load->library('upload', $config);  
				$this->upload->initialize($config);
		    	if ($this->upload->do_upload('DestinationImage'))
		    	{
		    		//echo "hii";exit;
					$data=array('DestinationName'=>$DestinationName,'DestinationImage'=>$DestinationImageName);
					//print_r($data);exit;
					$result=$this->DestinationModel->editDestination($data,$DestinationId);
					$data=array('upload_data'=>$this->upload->data());
					$data['navigationActive']="viewDestination";
					$data['destinationDetails']=$this->DestinationModel->viewDestination();
			    	redirect('AdminLoginControl/viewDestination');
		    	}
		    }
	    	else
	    	{	
	    		//echo "hi";exit;
		    	$data=array('DestinationName'=>$DestinationName,);
				$result=$this->DestinationModel->editDestination($data,$DestinationId);
				$data['navigationActive']="viewDestination";
				$data['destinationDetails']=$this->DestinationModel->viewDestination();
		    	//$this->load->view('viewDestination',$data);
		    	redirect('AdminLoginControl/viewDestination');
		    }
	    }
	    public function deleteDestination($DestinationId)
	    {
	    	//echo $PackageId;exit;
	    	//$vaildDeleteDestination=$this->vaildDeleteDestination($DestinationId);
	    	$result=$this->DestinationModel->deleteDestination($DestinationId);
			//$data["isDelete"]=$result;		
			$data['navigationActive']="viewDestination";
			$data['destinationDetails']=$this->DestinationModel->viewDestination();
			$this->session->set_flashdata('isDelete', $result);
			redirect('AdminLoginControl/viewDestination');
			//$this->load->view('viewDestination',$data);			
	    }
	}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class HomeSliderControl extends CI_Controller 
	{

		function __construct()
	    {
	    	parent::__construct();
	    	$this->load->model('AdminLoginModel');
	    	$this->load->model('DestinationModel');
	    	$this->load->model('PackageModel');
	    	$this->load->model('FlightModel');
	    	$this->load->model('HomeSliderModel');
	    	$this->load->model('HotelModel');
	        $this->load->library('session');
	        $this->load->library('upload');
	        $this->load->library('pagination');
	    }
	   	public function index()
	    {
	    	$data['navigationActive']="addHomeSlider";
	    	$this->load->view('addHomeSlider',$data);
	    } 
	    public function addHomeSlider()
	    {
	    	$HomeSliderTitle1=$this->input->post('txtHomeSlider1');
	    	$HomeSliderTitle2=$this->input->post('txtHomeSlider2');
	    	$HomeSliderTitle3=$this->input->post('txtHomeSlider3');
	    	$data=array('SliderTitle1'=>$HomeSliderTitle1,'SliderTitle2'=>$HomeSliderTitle2,'SliderTitle3'=>$HomeSliderTitle3);
	    	$returnId=$this->HomeSliderModel->addHomeSlider($data);
	    	$data["isAdd"]=$returnId;
			$data['navigationActive']="addHomeSlider";
	    	$this->load->view('addHomeSlider',$data);
	    }
	   	public function deleteHomeSlider($HomeSliderId)
	    {
	    	//echo $PackageId;exit;
	    	//$vaildDeleteDestination=$this->vaildDeleteDestination($DestinationId);
	    	$result=$this->HomeSliderModel->deleteHomeSlider($HomeSliderId);
			//$data["isDelete"]=$result;		
			$data['navigationActive']="viewHomeSlider";
			$data['homesliderDetails']=$this->HomeSliderModel->viewHomeSlider();
			$this->session->set_flashdata('isDelete', $result);
			redirect('AdminLoginControl/viewHomeSlider');
			//$this->load->view('viewDestination',$data);			
	    }
	    public function editHomeSlider($SliderId)
	    {
	    	$homesliderDetails=$this->HomeSliderModel->fetchEditHomeSliderDetails($SliderId);
	    	$data['homesliderDetails']=$homesliderDetails;
	    	$data['navigationActive']="viewHomeSlider";
	    	$this->load->view('editHomeSlider',$data);
	    }
	    public function editHomeSliderValidation()
	    {
	    	$HomeSliderTitle1=$this->input->post('txtHomeSlider1');
	    	$HomeSliderTitle2=$this->input->post('txtHomeSlider2');	
	    	$HomeSliderTitle3=$this->input->post('txtHomeSlider3');		
	    	$SliderId=$this->input->post('SliderId');
	    	$data=array('SliderTitle1'=>$HomeSliderTitle1,'SliderTitle2'=>$HomeSliderTitle2,'SliderTitle3'=>$HomeSliderTitle3);
			$result=$this->HomeSliderModel->editHomeSlider($data,$SliderId);
			$data['navigationActive']="viewHomeSlider";
			$data['homesliderDetails']=$this->HomeSliderModel->viewHomeSlider();
	    	//$this->load->view('viewDestination',$data);
	    	redirect('AdminLoginControl/viewHomeSlider');
	    }
	}
?>
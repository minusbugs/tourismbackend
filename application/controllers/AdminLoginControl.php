<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminLoginControl extends CI_Controller {

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('AdminLoginModel');
    	$this->load->model('DestinationModel');
    	$this->load->model('PackageModel');
    	$this->load->model('HotelModel');
    	$this->load->model('FlightModel');
    	$this->load->model('HomeSliderModel');
        $this->load->library('Session');
        $this->load->library('pagination');
        $this->load->model('AccountControlModel');
        $this->load->model('BookingModel');
    }

	public function index()
	{	
		$this->session->unset_userdata('AdminName');
		$this->session->unset_userdata('userType');
		$this->session->set_userdata('log','logout');
		$this->session->sess_destroy();
		$this->load->view('adminLoginForm');
	}

	public function adminLoginValidation()
	{
			$AdminName=$this->input->post('UserName');
			$AdminPassword=$this->input->post('Password');
			$count=$this->AdminLoginModel->adminLoginValidation($AdminName,$AdminPassword);
			
			if($count==1)
			{
				$userType=$this->AdminLoginModel->userType($AdminName);
				$this->session->set_userdata('AdminName',$AdminName);
				$this->session->set_userdata('userType',$userType);
				$this->session->set_userdata('log','login');
				$this->dashBoard();
			}
			else
			{
				$data['isLogin']=0;
				$this->session->unset_userdata('AdminName');
				$this->session->unset_userdata('userType');
				$this->session->set_userdata('log','logout');
				$this->session->sess_destroy();
				$this->load->view('adminLoginForm',$data);
			}
	}

	public function dashBoard()
	{
		///$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$countDestination=$this->AdminLoginModel->countDestination();
			$countPackages=$this->AdminLoginModel->countPackages();
			$countFlight=$this->FlightModel->countFlight();
			$countHotel=$this->HotelModel->countHotel();
			//echo $countDestination." ".$countPackages;exit;
			$data = array('totalDestinations'=>$countDestination,'totalPackages' =>$countPackages,'totalFlight' =>$countFlight,'totalHotel' =>$countHotel,'navigationActive'=>'dashBoard');
			$this->load->view('adminDashboard',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}	
	}
	public function addDestination()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data['navigationActive']="addDestination";
			$this->load->view('addDestination',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function addPackage()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data['DestinationName'] = $this->PackageModel->fetchDestinationSymbols();
			$data['navigationActive']="addPackage";
			$data['packid']=$this->PackageModel->getLastId();
			$this->load->view('addPackage',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function viewPackage()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewPackage";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewPackage";
			$total_row = $this->PackageModel->packageCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			$data["results"] = $this->PackageModel->fetch_data($config["per_page"], $page);
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->load->view('viewPackage',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function viewDestination()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewDestination";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewDestination";
			$total_row = $this->DestinationModel->destinationCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			// echo $page;exit;
			$data["results"] = $this->DestinationModel->fetch_data($config["per_page"], $page);
			//print_r($data);exit;
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			// View data according to array.
			//$this->load->view("pagination_view", $data);
			//$data['destinationDetails']=$this->DestinationModel->viewDestination();
			$this->load->view('viewDestination',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function changePassword()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isChange"]=$this->session->set_flashdata('isChange');
			$data['navigationActive']="changePassword";
			$this->load->view('changePassword',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}

	}
	public function addUser()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isAddUser"]=$this->session->set_flashdata('isAddUser');
			$data['navigationActive']="addUser";
			$this->load->view('addUser',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function viewUser()
	{
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewUser";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewUser";
			$total_row = $this->AccountControlModel->userCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			$data["results"] = $this->AccountControlModel->fetch_data($config["per_page"], $page);			
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->load->view('viewUser',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function addFlight()
	{
		if($this->session->has_userdata('log'))
		{
			$data["isAddUser"]=$this->session->set_flashdata('isAddUser');
			$data['navigationActive']="addFlight";
			$this->load->view('addFlight',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}	
	}
	public function viewFlight()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewFlight";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewFlight";
			$total_row = $this->FlightModel->flightCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 12;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			// echo $page;exit;
			$data["results"] = $this->FlightModel->fetch_data($config["per_page"], $page);
			//print_r($data);exit;
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			// View data according to array.
			//$this->load->view("pagination_view", $data);
			//$data['destinationDetails']=$this->DestinationModel->viewDestination();
			$this->load->view('viewFlight',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function addHotel()
	{
		if($this->session->has_userdata('log'))
		{
			$data["isAddUser"]=$this->session->set_flashdata('isAddUser');
			$data['navigationActive']="addHotel";
			$this->load->view('addHotel',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}	
	}
	public function viewHotel()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewHotel";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewHotel";
			$total_row = $this->HotelModel->hotelCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			// echo $page;exit;
			$data["results"] = $this->HotelModel->fetch_data($config["per_page"], $page);
			//print_r($data);exit;
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			// View data according to array.
			//$this->load->view("pagination_view", $data);
			//$data['destinationDetails']=$this->DestinationModel->viewDestination();
			$this->load->view('viewHotel',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function addHomeSlider()
	{
		if($this->session->has_userdata('log'))
		{
			$data["isAddUser"]=$this->session->set_flashdata('isAddUser');
			$data['navigationActive']="addHomeSlider";
			$this->load->view('addHomeSlider',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}	
	}
	public function viewHomeSlider()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data["isDelete"]=$this->session->flashdata('isDelete');
			$data['navigationActive']="viewHomeSlider";
			$config = array();
			$config["base_url"] = base_url()."index.php/AdminLoginControl/viewHomeSlider";
			$total_row = $this->HomeSliderModel->homesliderCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next';
			$config['prev_link'] = 'Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;
			}
			else
			{
				$page = 0;
			}
			// echo $page;exit;
			$data["results"] = $this->HomeSliderModel->fetch_data($config["per_page"], $page);
			//print_r($data);exit;
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );

			// View data according to array.
			//$this->load->view("pagination_view", $data);
			//$data['destinationDetails']=$this->DestinationModel->viewDestination();
			$this->load->view('viewHomeSlider',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('AdminName');
		$this->session->unset_userdata('userType');
		$this->session->set_userdata('log','logout');
		$this->session->sess_destroy();
		$this->load->view('adminLoginForm');
	}
	public function booking()
	{
		//$log=$this->session->userdata('log');
		if($this->session->has_userdata('log'))
		{
			$data['navigationActive']="viewBook";
			$config = array();
			$config["base_url"] = base_url()."AdminLoginControl/booking";
			$total_row = $this->BookingModel->bookingCount();
			$config["total_rows"] = $total_row;
			$config["per_page"] = 8;
			$config['use_page_numbers'] = TRUE;
			$config['num_links'] = 5;
			$config['cur_tag_open'] = '&nbsp;<a class="current">';
			$config['cur_tag_close'] = '</a>';
			$config['next_link'] = 'Next >';
			$config['prev_link'] = '< Previous';
			$this->pagination->initialize($config);
			if($this->uri->segment(3))
			{
				$page = ($this->uri->segment(3)) ;

			}
			else
			{
				$page = 0;
			}
			$data['page']=$page;
			$data["bookingDetails"] = $this->BookingModel->getBookingData($config["per_page"], $page);			
			$str_links = $this->pagination->create_links();
			$data["links"] = explode('&nbsp;',$str_links );
			$this->load->view('viewBooking',$data);
		}
		else
		{
			$this->session->unset_userdata('AdminName');
			$this->session->unset_userdata('userType');
			$this->session->set_userdata('log','logout');
			$this->session->sess_destroy();
			$this->load->view('adminLoginForm');
		}
	}
}
?>
<style type="text/css">
 .pagination li a.current
	{
		color: #ffffff;
    	background-color: #259dab;
	}
</style>

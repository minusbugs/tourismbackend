<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountControl extends CI_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('AdminLoginModel');
    	$this->load->model('AccountControlModel');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function changePasswordValidation()
    {
    	$currentPassword=$this->input->post('currentPassword');
    	$newPassword=$this->input->post('newPassword');
		$confirmNewPassword=$this->input->post('confirmNewPassword');
		if($confirmNewPassword!=$newPassword)
		{
    		$data['navigationActive']="changePassword";
    		$data['isChange']=0;
    		$this->load->view('changePassword',$data);     
		}
		else
		{
			$validOldPassword=$this->AccountControlModel->fetchOldPassword($currentPassword);
			if($validOldPassword!=1)
			{
				$data['navigationActive']="changePassword";
	    		$data['isChange']=0;
	    		$this->load->view('changePassword',$data); 
			}
			else
			{
				$return=$this->AccountControlModel->changePassword($newPassword);
				if($return==1)
				{
					$data['navigationActive']="changePassword";
		    		$data['isChange']=1;
		    		$this->load->view('changePassword',$data);
		    	} 
			}
		}
    }
    public function addUserValidation()
    {
    	$newUserName=$this->input->post('newUserName');
    	$newUserPassword=$this->input->post('newUserPassword');
    	$newUserConfirmPassword=$this->input->post('confirmNewUserPassword');
    	$newUserEmail=$this->input->post('newUserEmail');
    	$newUserContactNumber=$this->input->post('newUserContactNumber');
    	if($newUserConfirmPassword!=$newUserPassword)
		{
    		$data['navigationActive']="addUser";
    		$data['isAddUser']=2;
    		$data['errorMessage2']="The Password and Confirm password should be match";
    		$this->load->view('addUser',$data);     
		}
		else
		{
			$this->form_validation->set_rules('newUserName','User Name','is_unique[admin_db.AdminName]');	
			if($this->form_validation->run()==false)
			{
				$data['navigationActive']="addUser";
	    		$data['isAddUser']=3;
	    		$data['errorMessage3']="There is another user with same name.Try different username.";
	    		$this->load->view('addUser',$data);     	
			}
			else if(strlen($newUserContactNumber)<"10")
			{
				$data['navigationActive']="addUser";
	    		$data['isAddUser']=4;
	    		$data['errorMessage4']="The mobile number should be 10 digit.";
	    		$this->load->view('addUser',$data);
			}
			else
			{
				$data=array('AdminName'=>$newUserName,'AdminEmail'=>$newUserEmail,'AdminContactNumber'=>$newUserContactNumber,'AdminPassword'=>$newUserPassword,'UserType'=>"Guest");
				$returnId=$this->AccountControlModel->addUser($data);
				if($returnId==1)
				{
					$data['successMessage']="New User added.The previlage sholud be transfered.";
				}
				else
				{
					$returnId=5;
					$data['errorMessage5']="Server error. Try again later.";
				}
				$data["isAddUser"]=$returnId;
				$data['navigationActive']="addUser";
	    		$this->load->view('addUser',$data);	
			}
		}
    }
    public function deleteUser($AdminId)
	{
		$result=$this->AccountControlModel->deleteUser($AdminId);		
		$this->session->set_flashdata('isDelete', $result);
		redirect('AdminLoginControl/viewUser');			
	}
}
?>
//mobile number validation of addUser
function numbervalidate_addUser(textbox, e) 
{

  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57)) 
  {
    document.getElementById("addUsernumber").innerHTML = "Numbers only";
    $("#newUserContactNumber").css({
      "border": "1px solid red",
    });
    return false;
  } 
  else 
  {
    document.getElementById("addUsernumber").innerHTML = "";
    $("#newUserContactNumber").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

function numbervalidate_addUser1(textbox, e) 
{
  var contact = $('#newUserContactNumber').val().length;
 if (contact<10) 
 {
    document.getElementById("addUsernumber").innerHTML = "input 10 numbers";
    $("#newUserContactNumber").css({
    "border": "1px solid red",
    });
    return false;
 }
}
//pop up details in view_package_page
function popup_package(id) 
{
    var package_title="packagetitle_"+id;
    var package_title1=document.getElementById(package_title).textContent;
    var packagedescription="packagedescription_"+id;
    var packagedescription1=document.getElementById(packagedescription).textContent;
    var packageimage="packageimage_"+id;
    var packageimage1=document.getElementById(packageimage).src;
    $("#myModalLabel_1").text(id);
    $("#myModalLabel_head").text(package_title1);
    $("#myModalLabel_description").text(packagedescription1);
    document.getElementById("myModalLabel_image").src = packageimage1;
    $("#myModalView").modal('show');    
}

//pop up details in view_destination_page
function popup_destination(id) 
{
    var destination_title="destinationtitle_"+id;
    var destination_title1=document.getElementById(destination_title).textContent;
    var destinationimage="destinationimage_"+id;
    var destinationimage1=document.getElementById(destinationimage).src;
    $("#myModalLabel_1").text(id);
    $("#myModalLabel_head").text(destination_title1);    
    document.getElementById("myModalLabel_image").src = destinationimage1;
    $("#myModalView").modal('show');    
}